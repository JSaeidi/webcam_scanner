import numpy as np
import cv2
from time import sleep 
from glob import glob
import os.path
from PyQt5.QtWidgets import QFileDialog, QApplication, QMainWindow
"""
help:
spacebar for take a photo
Enter for savign photo
"""


class MainWindow(QMainWindow):
    def open_and_save(self):
        filter_type = """mono+inv(*.jpg);;mono(*.jpg);;inv(*.jpg);;original(*.jpg)"""
        #   mono+inv(*.jpg) -->     black_and_white=True; invert=True;
        #   mono(*.jpg)     -->     black_and_white=True; invert=False;
        #   inv(*.jpg)      -->     black_and_white=False; invert=True;
        #   original(*.jpg) -->     black_and_white=False; invert=False;
        maximum_number = 1
        for this_name in glob(f"{config.default_dir}/*.jpg.jpg"):
            print("for this:", this_name)
            this_file_name = os.path.split(this_name)[-1].split(".jpg.jpg")[0]
            if this_file_name.split(".jpg.jpg")[0].isdigit():
                print("first if:", this_name)
                if int(this_file_name.split(".jpg.jpg")[0]) >= maximum_number:
                    print("2nd if:", this_name)
                    maximum_number = int(this_file_name.split(".jpg.jpg")[0]) + 1
                    print("this name set:", maximum_number)

        path_to_save = os.path.join(config.default_dir, str(maximum_number))
        print("path_to_save", path_to_save)
        path = QFileDialog.getSaveFileName(self, 'save image', path_to_save,filter_type)

        if path[0]:
            saved_path = os.path.split(path[0])[:-1]
            config.default_dir = os.path.join(*saved_path)


        #print(path)
        return path


class Trapezoid:

    def __init__(self, img_size, r=5, color=(143, 149, 47),
                bottom_color=(196, 196, 196), border_color=(255, 0, 135)):
        # Initialize the contours
        self.contours = np.array([[[0 + r, 0 + r]],
                               [[0 + r, img_size[1] - r]],
                               [[img_size[0] - r, img_size[1] - r]],
                               [[img_size[0] - r, 0 + r]]])

        # Initialize the radius of the borders
        self.r = r
        # Initialize the colors of the trapezoid
        self.color = color
        self.bottom_color = bottom_color
        self.border_color = border_color

    def get_border_index(self, coord):
        # A border is return if the coordinates are in its radius
        for i, b in enumerate(self.contours[:, 0, :]):
            dist = sum([(b[i] - x) ** 2 for i, x in enumerate(coord)]) ** 0.5
            if  dist < self.r:
                return i
        # If no border, return None
        return None

    def set_border(self, border_index, coord):
        self.contours[border_index, 0, :] = coord
        
        
class Scanner():
    default_dir = ""

    def __init__(self, input_cv, output_path):
        #self.input = cv2.imread(input_path)
        self.input = input_cv
        self.output_path = output_path
        # get the shape and size of the input
        self.shape = self.input.shape[:-1]
        self.size = tuple(list(self.shape)[::-1])

        # create a trapezoid to drag and drop and its perspective matrix
        self.M = None
        self.trapezoid = Trapezoid(self.size,
                                r=min(self.shape) // 100 + 2,
                                color=(153, 153, 153),
                                border_color=(255, 0, 136),
                                bottom_color=(143, 149, 47))

        # Initialize the opencv window
        cv2.namedWindow('Rendering', cv2.WINDOW_NORMAL)
        cv2.setMouseCallback('Rendering', self.drag_and_drop_border)

        # to remember wich border is dragged if exists
        self.border_dragged = None
        
    def draw_trapezoid(self, img):
        # draw the contours of the trapezoid
        cv2.drawContours(img, [self.trapezoid.contours], -1,
                       self.trapezoid.color, self.trapezoid.r // 3)
        # draw its bottom
        cv2.drawContours(img, [self.trapezoid.contours[1:3]], -1,
                       self.trapezoid.bottom_color, self.trapezoid.r // 3)
        # Draw the border of the trapezoid as circles
        for x, y in self.trapezoid.contours[:, 0, :]:
            cv2.circle(img, (x, y), self.trapezoid.r,
                      self.trapezoid.border_color, cv2.FILLED)
        return img

    def drag_and_drop_border(self, event, x, y, flags, param):
        # If the left click is pressed, get the border to drag
        if event == cv2.EVENT_LBUTTONDOWN:
            # Get the selected border if exists
            self.border_dragged = self.trapezoid.get_border_index((x, y))

        # If the mouse is moving while dragging a border, set its new positionAxel THEVENOT
        elif event == cv2.EVENT_MOUSEMOVE and self.border_dragged is not None:
                self.trapezoid.set_border(self.border_dragged, (x, y))

        # If the left click is released
        elif event == cv2.EVENT_LBUTTONUP:
            # Remove from memory the selected border
            self.border_dragged = None
            
    def actualize_perspective_matrices(self):
        # get the source points (trapezoid)
        src_pts = self.trapezoid.contours[:, 0].astype(np.float32)

        # set the destination points to have the perspective output image
        h, w = self.shape
        dst_pts = np.array([[0, 0],
                          [0, h - 1],
                          [w - 1, h - 1],
                          [w - 1, 0]], dtype="float32")
 
        # compute the perspective transform matrices
        self.M = cv2.getPerspectiveTransform(src_pts, dst_pts)
        
    def run(self):
        while True:
            self.actualize_perspective_matrices()

            # get the output image according to the perspective transformation
            img_output = cv2.warpPerspective(self.input, self.M, self.size)

            # draw current state of the trapezoid
            img_input = self.draw_trapezoid(self.input.copy())
            # Display until the 'Enter' key is pressed
            cv2.imshow('Rendering', np.hstack((img_input, img_output)))
            #cv2.imshow('Rendering', img_input)
            if cv2.waitKey(1) & 0xFF == 13:
                break
 
        # Save the image and exit the process
        window = MainWindow()
        save_file = window.open_and_save()
        
        if   save_file[1] == 'mono+inv(*.jpg)' :
            grayImage = cv2.cvtColor(img_output, cv2.COLOR_BGR2GRAY)
            img_output = cv2.bitwise_not(grayImage)
            
        elif save_file[1] == 'mono(*.jpg)' :
            img_output = cv2.cvtColor(img_output, cv2.COLOR_BGR2GRAY)
            
        elif save_file[1] == 'inv(*.jpg)' :
            img_output = cv2.bitwise_not(img_output)
            
        elif save_file[1] == 'original(*.jpg)' :
            pass
        
        
        if save_file[1]:
            print(save_file[0])
            cv2.imwrite(save_file[0]+'.jpg', img_output)


        cv2.destroyAllWindows()
        
    def bonus(self, img):
        """Make the output better"""
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        value = hsv[..., 2]
        value = (value - np.min(value)) / (np.max(value) - np.min(value))
        hsv[..., 2] = (value * 255).astype(np.uint8)
        return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR) 



def click_event(event, x, y, flags, params):
    global img, crop_location
  
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
  
        # displaying the coordinates
        # on the Shell
        print(x, ' ', y)
        crop_location.append([x,y])
  
        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        #cv2.putText(img, str(x) + ',' +
        #            str(y), (x,y), font,
        #            1, (255, 0, 0), 2)
        img = cv2.circle(img, (x,y), 10, (0,0,255), -1)
        cv2.imshow('image', img)
  
    # checking for right mouse clicks     
    if event==cv2.EVENT_RBUTTONDOWN:
  
        # displaying the coordinates
        # on the Shell
        print(x, ' ', y)
  
        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        b = img[y, x, 0]
        g = img[y, x, 1]
        r = img[y, x, 2]
        #cv2.putText(img, str(b) + ',' +
        #            str(g) + ',' + str(r),
        #            (x,y), font, 1,
        #            (255, 255, 0), 2)

        cv2.imshow('image', img)


def show_webcam(mirror=False, webcam_number=3):
    global ret, frame
    cam = cv2.VideoCapture(webcam_number)
    cv2.namedWindow("webcam", cv2.WINDOW_AUTOSIZE) 
    flag = True
    while True:
        ret_val, img = cam.read()
        #sleep(0.1)

        if mirror: 
            img = cv2.flip(img, -1)
#            img = cv2.flip(img, 1)
            pass
        grayImage = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        imagem = cv2.bitwise_not(grayImage)
        image_rotated = cv2.rotate(imagem, cv2.ROTATE_90_CLOCKWISE)
        imS = cv2.resize(image_rotated, (660, 880))
        cv2.imshow('webcam', imS)
        #cv2.setMouseCallback('webcam', click_event)
        key = cv2.waitKey(1)
        if key == 27: 
            print("goodbye")
            break  # esc to quit
        elif key == ord(' '):
            print("press!!!!")
            ro = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
            resi = cv2.resize(ro, (630, 840))
            scn = Scanner(resi, "./outut_12345.jpg")
            scn.run()
            #while True:
            #    if cv2.waitKey(1) == ord(' '):
            #        break
            #    sleep(0.1)


    #cv2.destroyAllWindows()



class config():
    default_dir = ""



if __name__ == '__main__':
    config.default_dir = os.path.expanduser("~")
    print("config().default_dir", config().default_dir)
    app = QApplication(['save to'])
    img = show_webcam(mirror=True, webcam_number=0)

